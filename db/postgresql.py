import postgresql
db = postgresql.open('pq://postgres:1Qaz2wsx@db-core.cmhmufopqm9i.ap-northeast-1.rds.amazonaws.com:5432/cryto')
def storeData(data):
    db.execute("""CREATE TABLE IF NOT EXISTS WhaleSniper (
        send_date date, message_raw text,
        market text, coin text, signal_type text, 
        trade_amount numeric, trade_duration numeric, trade_percent numeric, 
        bid_price numeric, bid_trend numeric, bid_percent numeric,
        ask_price numeric, ask_trend numeric, ask_percent numeric,
        l_price numeric, l_trend numeric, l_percent numeric,
        last_signal text, vol_in_24h numeric)
    """)
    make_emp = db.prepare("INSERT INTO WhaleSniper VALUES ($1, $2, $3,$4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19)")
    make_emp(
        data["send_date"],
        data["message_raw"],
        data["market"],
        data["coin"],
        data["signal_type"],
        data["trade_amount"],
        data["trade_duration"],
        data["trade_percent"],
        data["bid_price"],
        data["bid_trend"],
        data["bid_percent"],
        data["ask_price"],
        data["ask_trend"],
        data["ask_percent"],
        data["l_price"],
        data["l_trend"],
        data["l_percent"],
        data["last_signal"],
        data["vol_in_24h"]
        )
    