# -*- coding: utf-8 -*-

from telethon import TelegramClient, sync
import socks
import asyncio
from pprint import pprint
# from db.postgresql import storeData
from db.mongo import storeData
from datetime import datetime, timedelta
import pathlib
# Telegram config
api_id = 3260835 # Your api_id
api_hash = "77a0bbaf9c23bfa49cd27279c70521f8" # Your api_hash
# session_name = str(pathlib.Path().absolute())+'/'+'xx.session' # 
session_name = '/opt/logs/xx' # 
# bot_token='1781842052:AAFr3fCp4pq-u7LSHRchoy3a57JOqy4Olro'
# proxy_param = (socks.SOCKS5, 'localhost', 1080) # Proxy settings, if you need

# Create connection
client = TelegramClient(session_name,api_id, api_hash).start()
# client = TelegramClient("bot", api_id, api_hash).start(bot_token=bot_token)
def conver_data_amount(amount):
    if amount[-1] == "K":
        amount = float(amount[:1])*100
    elif amount[-1] == "M":
        amount = float(amount[:1])*1000
    elif amount[-1] == "B":
        amount = float(amount[:1])*1000000
    else:
        amount = float(amount)

async def main():
    channel = await client.get_entity('@WhaleSniper')
    date = datetime.utcnow() - timedelta(minutes= 5)
    pprint(date)
    messages = await client.get_messages(channel , limit=1000, search="Last signal")
    #then if you want to get all the messages text
    datas = []
    for x in messages:
        # pprint(x.message)
        if x.date.replace(tzinfo=None) < date.replace(tzinfo=None):
            return
        if not x.message:
            pprint(vars(x))
            continue
        data = {
            "send_date" : x.date,
            "message_raw" : x.message
        }
        # Process messgae
        msg_list = x.message.splitlines()
        data["market"] = msg_list[0].split()[0]
        data["coin"] = msg_list[1].split()[0][1:]
        data["signal_type"] = msg_list[1].split()[2]
       
        data["trade_amount"] = conver_data_amount(msg_list[2].split()[3]) 

        data["trade_duration"] = conver_data_amount(msg_list[2].split()[3]) 
        data["trade_percent"] = conver_data_amount(msg_list[2].split()[5][1:][:2])

        if msg_list[3][0] == "B":
            data["bid_price"] = conver_data_amount(msg_list[3].split()[1])
            bid_trend = msg_list[3].split()[2]
            data["bid_trend"] = -1 if bid_trend=="🔴" else 1 if bid_trend=="❇" else 0
            data["bid_percent"] = conver_data_amount(msg_list[3].split()[3][1:][:2])


            data["ask_price"] = conver_data_amount(msg_list[4].split()[1])
            ask_trend = msg_list[4].split()[2]
            data["ask_trend"] = -1 if ask_trend=="🔴" else 1 if ask_trend=="❇" else 0
            data["ask_percent"] = conver_data_amount(msg_list[4].split()[3][1:][:2])

            data["l_price"] = 0
            data["l_trend"] = 0
            data["l_percent"] = 0
            
            data["last_signal"] = msg_list[5].split(":")[1]
            data["vol_in_24h"] = conver_data_amount(msg_list[5].split()[2])
        elif msg_list[3][0] == "L":
            data["l_price"] = conver_data_amount(msg_list[3].split()[1])
            l_trend = msg_list[3].split()[2]
            data["l_trend"] = -1 if l_trend=="🔴" else 1 if l_trend=="❇" else 0
            data["l_percent"] = conver_data_amount(msg_list[3].split()[3][1:][:2])

            data["bid_price"] = 0
            data["bid_trend"] = 0
            data["bid_percent"] = 0
            data["ask_price"] = 0
            data["ask_trend"] = 0
            data["ask_percent"] = 0


            data["last_signal"] = msg_list[4].split(":")[1]
            data["vol_in_24h"] = conver_data_amount(msg_list[4].split()[2])

        datas.append(data)
        # pprint(data)
        storeData(data)
        # print("-----------------------------")
    
loop = asyncio.get_event_loop()
loop.run_until_complete(main())